import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import './App.css'
import PokeList from './PokeList';
import Pokedex from './Pokedex';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const COLORS = {
  Psychic: "#f8a5c2",
  Fighting: "#f0932b",
  Fairy: "#c44569",
  Normal: "#f6e58d",
  Grass: "#badc58",
  Metal: "#95afc0",
  Water: "#3dc1d3",
  Lightning: "#f9ca24",
  Darkness: "#574b90",
  Colorless: "#FFF",
  Fire: "#eb4d4b"
}

class App extends Component {


  componentDidMount(){
      console.log(PokeList);

  }

  render() {
    return (
      <div className="App"><h2 className="text-center">Pokedex</h2>
        <Pokedex/>
      </div>
    )
  }
}

export default App
