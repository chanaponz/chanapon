import React, { Component } from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Row, Col } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import PokeList from './PokeList';

class PopupModal extends Component {
	constructor(props){
		super(props);
		const {isOpen, onUpdate, onToggle} = this.props;
		this.state = {
			pokemon : PokeList.cards
		}

		this.options = {
			onRowClick: (row) => {
				const { pokemon } = this.state;
				const filtered = pokemon.filter((value)=>{
					return value.id !== row.id;
				});
				
				this.setState({
					pokemon: filtered
				},()=>onUpdate(row));
			}
		}
	}

	componentDidMount(){
		//filter with existed list
		
	}

	getList = () => {
		const { existed } = this.props;
		const pokemon = PokeList.cards;
		return pokemon;
	}

	filterName = (event) => {
		const selected = event.target.value;
		if( selected === '' ){
			const list = this.getList();
			this.setState({
				pokemon: list
			});
			return false;
		}
		const { pokemon } = this.state;
		const filtered = pokemon.filter((value)=>{
			const result = value.name.indexOf(selected);
			return result >= 0;
		});
		this.setState({
			pokemon: filtered
		});
	}

	getcard = (cell, row) => {
		return(
			<Row>
				<img src={row.imageUrl} alt={"img-"+row.imageUrl} className=" "/>
				<Col><h3>{ row.name }</h3>
				<p>HP { cell.hp }</p>
				<p>STR</p>
				<p>Weak</p>
				<p>cute</p>
				<p className="position-absolute top-right">ADD</p>
				</Col>
			</Row>
			);
	}

	render() {
		const {isOpen, onUpdate, onToggle} = this.props;
		const { pokemon } = this.state;
		return(
			<Modal isOpen={isOpen} toggle={onToggle} className="modal-lg">
				<ModalBody >
					<input type="text" onChange={this.filterName}/>
					<BootstrapTable data={ pokemon } options={this.options} bordered={false} className="pokedex">
						<TableHeaderColumn isKey dataField='id' dataFormat ={this.getcard}></TableHeaderColumn>
	      			</BootstrapTable>
				</ModalBody>
			</Modal>
			);
	}
}

export default PopupModal;