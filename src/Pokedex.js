import React, { Component } from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Row, Col } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { object, oneOfType} from 'prop-types';
import PokeList from './PokeList';
import PopupModal from './PopupModal';
import plus from './img/plus-solid.svg';
class Pokedex extends Component {
	constructor(props){
		super(props);
		this.state = {
			existedPokemon: [],
			modal: false,
		}
	}

	componentDidMount(){

	}

	updateList = (pokemon) => {
		console.log('call update');
		const { existedPokemon } = this.state;
		existedPokemon.push(pokemon);
		this.setState({existedPokemon});
	}

	toggle = () =>{
		const { modal } = this.state;
		this.setState({modal : !modal});
		console.log('call toggle');
	}
	getcard = (cell, row) => {
		console.log(row);
		return(
			<Row>
				<img src={row.imageUrl} alt={"img-"+row.imageUrl} className=" "/>
				<Col><h3>{ row.name }</h3>
				<p>HP { cell.hp }</p>
				<p>STR</p>
				<p>Weak</p>
				<p>cute</p>
				<p className="position-absolute top-right">ADD</p>
				</Col>
			</Row>
			);
	}

	render(){
		const { existedPokemon, modal } = this.state;
		// if(existedPokemon.length === 0) return '';
		return(
			<div className="pokedex">
			<PopupModal isOpen={modal} onUpdate={this.updateList} onToggle={this.toggle} existed={existedPokemon}/>
			<BootstrapTable data={ existedPokemon }>
		       <TableHeaderColumn isKey dataField='id' dataFormat ={this.getcard}></TableHeaderColumn> 
	      	</BootstrapTable>
	      	<img src={plus} alt="plus" className="plus position-absolute" onClick={this.toggle}/>

			</div>
			);
	}
}

// Pokedex.propTypes = {
// 	pokemon: oneOfType([object]).required,
// };
export default Pokedex;